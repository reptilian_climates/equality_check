

'''
	https://docs.python.org/3/library/unittest.html#module-unittest
'''

'''
	python -m unittest *STATUS.py
'''


#STATUS = "LOCAL"
STATUS = "PYPI"

MODULES_PATHS = []
if (STATUS == "LOCAL"):
	MODULES_PATHS = [ 'THIS_MODULE' ]
if (STATUS == "PYPI"):
	MODULES_PATHS = [ 'THIS_MODULE_PIP' ]

def ADD_PATHS_TO_SYSTEM (PATHS):
	import pathlib
	FIELD = pathlib.Path (__file__).parent.resolve ()

	from os.path import dirname, join, normpath
	import sys
	for PATH in PATHS:
		sys.path.insert (0, normpath (join (FIELD, PATH)))

def FOLDER (PATH):
	import pathlib
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve () 

	from os.path import dirname, join, normpath
	return normpath (join (THIS_FOLDER, PATH))

ADD_PATHS_TO_SYSTEM ([ 'THIS_MODULE' ])

import sys
print (sys.path)

from equality_check import EQUALITY

import unittest

class CONSISTENCY (unittest.TestCase):
	def test_1 (THIS):
		print ("test_1")
		
		EQ = EQUALITY (
			FOLDER ("STATUS_FOLDERS/1/EQ_1"),
			FOLDER ("STATUS_FOLDERS/1/EQ_2")
		)
		
		assert (len (EQ ['1']) == 0)
		assert (len (EQ ['2']) == 0)	
		
		print (EQ)
		
	def test_2 (THIS):
		print ("test_2")
		
		EQ = EQUALITY (
			FOLDER ("STATUS_FOLDERS/2/EQ_1"),
			FOLDER ("STATUS_FOLDERS/2/EQ_2")
		)
		
		print (EQ)
		
		assert (EQ ['1'] == {
			'1.HTML': 'f', 
			'1/1.HTML': 'f'
		})
		
		assert (EQ ['2'] == {
			'2': 'd', 
			'2.HTML': 'f', 
			'1/2.HTML': 'f'
		})	
		



'''
if __name__ == '__main__':
	unittest.main ()
'''